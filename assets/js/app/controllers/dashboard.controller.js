define("App.Controller.Dashboard", function(module) {

  var DashboardService = require("App.Service.Dashboard");
  
  var DashboardController = Marionette.Object.extend({
    initialize: function(options){
      this.repo = options.repo;
      this.vent = options.vent;
      this.router = options.router;
      this.layout = options.layout;
      this.service = new DashboardService();

      console.log(options);
      
      this.listenTo(this.router, "route:showDashboard", this.showDashboard);      
      this.listenTo(this.router, "route:getDashboard", this.getDashboard);
    },

    getDashboard: function() {
      console.log('getting dashboard');
      var stuff = this.service.getDashboard();
      console.log(stuff);

    },

    showDashboard: function() {
        console.log('showing the dashboard');
    }
  });

  module.exports = DashboardController;
});